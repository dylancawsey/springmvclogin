package me.dylancawsey;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
public class HomeController {
    @GetMapping
    public ModelAndView home(@SessionAttribute String username) {
        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("username", username);

        return modelAndView;
    }
}