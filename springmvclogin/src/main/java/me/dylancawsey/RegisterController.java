package me.dylancawsey;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/register")
public class RegisterController {
    @PostMapping
    public ModelAndView register(HttpSession session, @RequestParam String username, @RequestParam String password) {
        session.setAttribute("username",username);

        User u = new User();
        u.setUsername(username);
        u.setPassword(password);

        EntityManager em = Persistence.createEntityManagerFactory("flightpub")
                .createEntityManager();
        em.getTransaction().begin();
        em.persist(u);
        em.getTransaction().commit();

        return new ModelAndView("redirect:/home");
    }

}
