package folder;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Map;

public class IndexAction extends ActionSupport implements SessionAware {
    private String username;
    private String password;
    private Map<String, Object> session;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        session.put("username", username);

        User u = new User();
        u.setUsername(this.username);
        u.setPassword(this.password);

        EntityManager em = Persistence.createEntityManagerFactory("flightpub")
                .createEntityManager();
        em.getTransaction().begin();
        em.persist(u);
        em.getTransaction().commit();


        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}