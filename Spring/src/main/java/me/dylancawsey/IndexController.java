package me.dylancawsey;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class IndexController {
    @GetMapping
    public ModelAndView index(@RequestParam(defaultValue = "Guest") String name) {
        ModelAndView view = new ModelAndView("index");
        view.addObject("name", name);
        return view;
    }
}

